# Project AURA


Notre solution se porte sur la détection et la reconnaissance de l’environnement destinées aux voitures autonomes. 

À l’aide de différents capteurs, notre système sera capable en temps-réel de fusionner les données, de détecter et de reconnaître les éléments suivants :

— Les véhicules

— Tous types de panneaux

— Les piétons

— Les lignes de route

Il sera aussi capable de percevoir la distance qui sépare la voiture des obstacles.